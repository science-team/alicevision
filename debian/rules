#!/usr/bin/make -f
# -*- makefile -*-

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

%:
	dh $@

# I'd like to turn this on, but I get build errors
USE_OCVSIFT := OFF

# ON produces errors. Leave it OFF for now
REQUIRE_CERES_WITH_SUITESPARSE := OFF

override_dh_auto_configure:
	dh_auto_configure -- \
		-DALICEVISION_BUILD_DEPENDENCIES=OFF \
		-DALICEVISION_BUILD_DOC=OFF \
		-DAV_USE_CUDA=OFF \
		-DAV_BUILD_CUDA=OFF \
		-DAV_BUILD_ZLIB=OFF \
		-DAV_BUILD_ASSIMP=OFF \
		-DAV_BUILD_TIFF=OFF \
		-DAV_BUILD_JPEG=OFF \
		-DAV_BUILD_PNG=OFF \
		-DAV_BUILD_LIBRAW=OFF \
		-DAV_BUILD_POPSIFT=OFF \
		-DAV_BUILD_CCTAG=OFF \
		-DAV_BUILD_APRILTAG=OFF \
		-DAV_BUILD_OPENGV=OFF \
		-DAV_BUILD_OPENCV=OFF \
		-DAV_BUILD_ONNXRUNTIME=OFF \
		-DAV_BUILD_LAPACK=OFF \
		-DAV_BUILD_SUITESPARSE=OFF \
		-DAV_BUILD_FFMPEG=OFF \
		-DAV_BUILD_VPX=OFF \
		-DAV_BUILD_COINUTILS=OFF \
		-DAV_BUILD_OSI=OFF \
		-DAV_BUILD_CLP=OFF \
		-DAV_BUILD_FLANN=OFF \
		-DAV_BUILD_LEMON=OFF \
		-DAV_BUILD_PCL=OFF \
		-DAV_BUILD_USD=OFF \
		-DAV_BUILD_GEOGRAM=OFF \
		-DAV_BUILD_TBB=OFF \
		-DAV_BUILD_EIGEN=OFF \
		-DAV_BUILD_EXPAT=OFF \
		-DAV_BUILD_OPENEXR=OFF \
		-DAV_BUILD_ALEMBIC=OFF \
		-DAV_BUILD_OPENIMAGEIO=OFF \
		-DAV_BUILD_BOOST=OFF \
		-DAV_BUILD_CERES=OFF \
		-DALICEVISION_USE_ONNX=OFF \
		-DALICEVISION_USE_CUDA=OFF \
		-DALICEVISION_USE_OPENMP=ON \
		-DALICEVISION_USE_OPENCV=ON \
		-DALICEVISION_USE_OPENCV_CONTRIB=OFF \
		-DALICEVISION_USE_OCVSIFT=$(USE_OCVSIFT) \
		-DALICEVISION_USE_APRILTAG=ON \
		-DALICEVISION_USE_OPENGV=ON \
		-DALICEVISION_USE_MESHSDFILTER=ON \
		-DALICEVISION_REQUIRE_CERES_WITH_SUITESPARSE=$(REQUIRE_CERES_WITH_SUITESPARSE) \
		-DALICEVISION_USE_RPATH=OFF \
		-DBUILD_SHARED_LIBS=ON \
		-DALICEVISION_BUILD_TESTS=ON \
		-DALICEVISION_BUILD_EXAMPLES=ON \
		-DLEMON_INCLUDE_DIR_HINTS=/usr/include/lemon/ \
		-DFLANN_INCLUDE_DIR_HINTS=/usr/include/flann/ \
		-DGEOGRAM_LIBRARY=/usr/lib/$(DEB_HOST_GNU_TYPE)/libgeogram.so.1 \
		-DGEOGRAM_INCLUDE_DIR=/usr/include/geogram1

# To make the tests pass I need to set ALICEVISION_ROOT to some directory inside
# the build tree. I get this as relative to this debian/rules file
override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	ALICEVISION_ROOT=$(dir $(realpath $(lastword $(MAKEFILE_LIST))))/../src/aliceVision/image dh_auto_test
endif

# The cmake files are arch-specific, so they should go into /usr/lib/ARCH not
# /usr/share
#
# Extra notes in /usr/share/aliceVision shouldn't live there, but they're not
# useful anyway, so I remove them
execute_after_dh_auto_install:
	mkdir -p debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/aliceVision
	mv debian/tmp/usr/share/aliceVision/cmake debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/aliceVision

	rm debian/tmp/usr/share/aliceVision/*.md
